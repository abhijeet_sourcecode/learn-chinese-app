package com.lang.chinese;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {

	private Button button_noun;
	private Button button_verb;
	private Button button_preposition;
	private Button button_adverbs;
	private Button button_adjectives;
	private Button button_pronouns;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choose_part);
		addListenerOnButton();
	}

	private void addListenerOnButton() {
		button_noun = (Button) findViewById(R.id.nouns);

		button_noun.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				final Intent myIntent = new Intent(MainActivity.this, Mode.class);
				myIntent.putExtra("part", 1);
				MainActivity.this.startActivity(myIntent);
			}

		});

		button_verb = (Button) findViewById(R.id.verbs);

		button_verb.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				final Intent myIntent = new Intent(MainActivity.this, Mode.class);
				myIntent.putExtra("part", 2);
				MainActivity.this.startActivity(myIntent);
			}

		});

		button_preposition = (Button) findViewById(R.id.prepositions);

		button_preposition.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				final Intent myIntent = new Intent(MainActivity.this, Mode.class);
				myIntent.putExtra("part", 3);
				MainActivity.this.startActivity(myIntent);
			}

		});

		button_adverbs = (Button) findViewById(R.id.adverbs);

		button_adverbs.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				final Intent myIntent = new Intent(MainActivity.this, Mode.class);
				myIntent.putExtra("part", 4);
				MainActivity.this.startActivity(myIntent);
			}

		});

		button_adjectives = (Button) findViewById(R.id.adjectives);

		button_adjectives.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				final Intent myIntent = new Intent(MainActivity.this, Mode.class);
				myIntent.putExtra("part", 5);
				MainActivity.this.startActivity(myIntent);
			}

		});

		button_pronouns = (Button) findViewById(R.id.pronouns);

		button_pronouns.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				final Intent myIntent = new Intent(MainActivity.this, Mode.class);
				myIntent.putExtra("part", 6);
				MainActivity.this.startActivity(myIntent);
			}

		});
	}

}
