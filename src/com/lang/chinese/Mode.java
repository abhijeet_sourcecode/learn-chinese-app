package com.lang.chinese;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class Mode extends Activity {

	private int part;
	private Button button_memrise;

	private Map<Integer, String> meaning = null;
	private ListView lv_recall;
	private ListView lv_memrise;
	private Button button_recall;
	private Context context;

	int duration = Toast.LENGTH_SHORT;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choose_mode);

		final Bundle b = getIntent().getExtras();
		part = b.getInt("part");
		Log.d("langmem", "" + part);

		if (meaning == null) {
			meaning = new HashMap<Integer, String>();
		}
		addListenerOnButton_memrise();
		addListenerOnButton_recall();

		context = getApplicationContext();

	}

	private void addListenerOnButton_recall() {
		button_recall = (Button) findViewById(R.id.Recall);

		button_recall.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				switch (part) {
				case 1:
					populate_2(R.raw.nouns);

					break;

				case 2:
					populate_2(R.raw.verbs);

					break;

				case 3:
					populate_2(R.raw.prepositions);

					break;

				case 4:
					populate_2(R.raw.adverbs);

					break;

				case 5:
					populate_2(R.raw.adjectives);

					break;

				case 6:
					populate_2(R.raw.pronouns);

					break;

				default:
					break;
				}

			}

		});

	}

	@Override
	public void onResume() {
		super.onResume();

	}

	@Override
	public void onPause() {

		meaning = null;

		super.onPause();

	}

	private void addListenerOnButton_memrise() {
		button_memrise = (Button) findViewById(R.id.memorise);

		button_memrise.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				switch (part) {
				case 1:
					populate_1(R.raw.nouns);

					break;

				case 2:
					populate_1(R.raw.verbs);

					break;

				case 3:
					populate_1(R.raw.prepositions);

					break;

				case 4:
					populate_1(R.raw.adverbs);

					break;

				case 5:
					populate_1(R.raw.adjectives);

					break;
				case 6:
					populate_1(R.raw.pronouns);

				default:
					break;
				}

			}

		});

	}

	private void populate_2(int args) {

		final List<String> display_list = new ArrayList<String>();
		final InputStream is = getResources().openRawResource(args);
		final BufferedReader in = new BufferedReader(new InputStreamReader(is));
		int count = 0;
		String line = null;
		try {
			while ((line = in.readLine()) != null) {
				line = line.trim();
				if (line.length() > 0) {

					String key = line;
					String value = "";
					final int x = line.indexOf(":");
					if (x != -1) {
						key = line.substring(0, x);
						value = line.substring(x + 1, line.length());
						// Log.i("lang", "puting " + count + " for " + value);
						meaning.put(count, value);
						count++;
					}

					display_list.add(key);
				}

			}
			in.close();
			is.close();
		} catch (final IOException e) {

			e.printStackTrace();
		}

		setContentView(R.layout.recaller);
		lv_recall = (ListView) findViewById(R.id.torecalllist);

		final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.lvcenter, display_list);

		lv_recall.setAdapter(arrayAdapter);

		lv_recall.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

				Log.i("langmem", position + "");
				String means = meaning.get(position);
				if (means == null) {
					means = "i can't seem to remember as well !";
				}

				final Toast toast = Toast.makeText(context, means, duration);
				toast.show();

			}

		});

	}

	private void populate_1(int args) {

		final List<String> display_list = new ArrayList<String>();
		final InputStream is = getResources().openRawResource(args);
		final BufferedReader in = new BufferedReader(new InputStreamReader(is));

		String line = null;
		try {
			while ((line = in.readLine()) != null) {
				line = line.trim();
				if (line.length() > 0) {
					display_list.add(line.replace(":", " : "));
				}
			}
			in.close();
			is.close();
		} catch (final IOException e) {

			e.printStackTrace();
		}

		setContentView(R.layout.memoriser);
		lv_memrise = (ListView) findViewById(R.id.tomemlist);

		final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.lvcenter, display_list);

		lv_memrise.setAdapter(arrayAdapter);

	}
}
